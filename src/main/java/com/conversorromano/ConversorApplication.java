package com.conversorromano;

import java.util.Scanner;

import static java.util.Collections.nCopies;
import static java.lang.String.join;

public class ConversorApplication {

	Integer numero;
	String numeroRomano;

	public ConversorApplication() {
	}

	public ConversorApplication(Integer numero, String numeroRomano) {
		this.numero = numero;
		this.numeroRomano = numeroRomano;
	}

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public String getNumeroRomano() {
		return numeroRomano;
	}

	public void setNumeroRomano(String numeroRomano) {
		this.numeroRomano = numeroRomano;
	}

	public int obtemNumero() {
		Scanner leia = new Scanner(System.in);
		System.out.print("Informe um numero entre 1 e 3999:");
		int num = leia.nextInt();
		return num;
	}


	public String converteRomano(int numero) {
/*
		private static final int[] DECIMAIS =
				{1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
		private static final String[] ROMANOS =
				{"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

		StringBuilder resultado = new StringBuilder();
				for (int i = 0; i < DECIMAIS.length; i++) {
					int parteInteira = numero / DECIMAIS[i];
					numero -= DECIMAIS[i] * parteInteira;
					resultado.append(join("", nCopies(parteInteira, ROMANOS[i])));
				}
				numeroRomano = resultado.toString(); */

		int sobra = 0;
		String roma = "";

		//para 1000 ->M
		if (numero < 4000) {
			while (numero >= 1000) {
				roma += "M";
				numero -= 1000;
			}
			//para 900 ->CM
			while (numero >= 900) {
				roma += "CM";
				numero -= 900;
			}
			//para 500 -> D
			while (numero >= 500) {
				roma += "D";
				numero -= 500;
			}
			//para 400 -> CD
			while (numero >= 400) {
				roma += "CD";
				numero -= 400;
			}
			//para 100 -> C
			while (numero >= 100) {
				roma += "C";
				numero -= 100;
			}
			//para 90 -> XC
			while (numero >= 90) {
				roma += "XC";
				numero -= 90;
			}
			//para 50 -> L
			while (numero >= 50) {
				roma += "L";
				numero -= 50;
			}
			//para 40 -> XL
			while (numero >= 40) {
				roma += "XL";
				numero -= 40;
			}
			//para 10 -> X
			while (numero >= 10) {
				roma += "X";
				numero -= 10;
			}
			//para 9 -> IX
			while (numero >= 9) {
				roma += "IX";
				numero -= 9;
			}
			//para 5 -> V
			while (numero >= 5) {
				roma += "V";
				numero -= 5;
			}
			//para 4 -> IV
			while (numero >= 4) {
				roma += "IV";
				numero -= 4;
			}
			//para 1 -> I
			while (numero >= 1) {
				roma += "I";
				numero -= 1;
			}
		}
		return roma;
	}
}