package com.conversorromano;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class ConversorApplicationTests {

	@Test
	public void testeConverteRomano(){
		ConversorApplication conversor = new ConversorApplication();

        int numero = 10;
        Assertions.assertEquals(conversor.converteRomano(numero), "X");

		int numero4 = 500;
		Assertions.assertEquals(conversor.converteRomano(numero4), "D");

		int numero5 = 25;
		Assertions.assertEquals(conversor.converteRomano(numero5), "XXV");

		int numero2 = 0;
		Assertions.assertEquals(conversor.converteRomano(numero2), "");

		int numero3 = 12;
		Assertions.assertEquals(conversor.converteRomano(numero3), "XII");

	}
}
